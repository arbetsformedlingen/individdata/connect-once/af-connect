// portability-api.js
const config = require("../config");
const axios = require("axios");

function cv(ssoCookie, sessionToken) {
  const url = config.portabilityUrl + "/profile?sessionToken=" + sessionToken;
  return axios.get(url, {
    headers: {
      AMV_SSO_COOKIE: ssoCookie,
      host: config.portabilityHost
    }
  }).then(response => {
    // Read response from Portability api and extract the CV
    if (response.status === 200) {
      return response.data;
    } else {
      throw "failed to get cv from portabilty";
    }
  });
}

function store(ssoCookie, data) {
  const url = config.portabilityUrl + "/store";
  return axios.post(url, {
    token: ssoCookie,
    value: JSON.stringify(data)
  }, {
    headers: {
      host: config.portabilityHost
    },
  }).then(response => {
    // Read response from Portability api and extract the CV
    if (response.status === 200) {
      return response.data;
    } else {
      throw "failed to save cv";
    }
  });
}

module.exports = {
  cv: cv,
  store: store
};

FROM node:18.19.0-alpine3.19

## Is used as proxy before node server
USER root
ARG ARG_BUILDNAME \
    ARG_USER=default \
    ARG_PASSWD=default

ENV buildName=$ARG_BUILDNAME \
    USER=$ARG_USER \
    PASSWD=$ARG_PASSWD

RUN echo 'kolla:' ${USER} ${PASSWD} ' buildName:'${buildName}  &&\
    apk update &&\
    apk add --no-cache --update -v \ 
        curl \
        supervisor \
        nginx \
        git
        
WORKDIR /dist
#COPY package.json /app
COPY . .
RUN npm install -g npm@9.8.1 &&\
    npm install &&\
    npm update &&\
    npm run build

#Create Document root
RUN mkdir /opt/nginx &&\
    mkdir /opt/nginx/www &&\
#Should be empty
    ls -la /opt/nginx/www


ENV TZ=Europe/Stockholm
RUN date +"%Y-%m-%dT%H:%M:%S %Z" &&\
    apk add -q apache2-utils && htpasswd -dbc /etc/nginx/htpasswd $USER $PASSWD &&\
    rm -rf /var/cache/apk/* &&\
#Move config to image
    mkdir /tmp/conf
COPY prod-nginx.conf /tmp/conf
COPY stage-nginx.conf /tmp/conf

#Determinate which configuration we should use
RUN if [ "$buildName" = stage ];\
 then echo "Doing a Stage build" ;\
    rm -f /tmp/conf/prod-nginx.conf;\
    mv /tmp/conf/stage-nginx.conf /etc/nginx/nginx.conf;\
 else echo "Doing a Production build";\
     rm -f /tmp/conf/stage-nginx.conf;\
     mv /tmp/conf/prod-nginx.conf /etc/nginx/nginx.conf ;\
 fi
########
RUN rm -f /etc/nginx/conf.d/default.conf
COPY ./supervisord.conf /etc/supervisord.conf
########
RUN mkdir -p /var/run/nginx &&\
    chmod -R 777 /var/run/nginx &&\
    mkdir -p /var/run/supervisord /var/log/supervisord &&\
    chmod -R 777 /var/run/supervisord  &&\
    apk add --no-cache bash &&\
    chmod -R 777 /var/log &&\
    chmod -R 777 /var/lib/nginx

#######
EXPOSE 3000 8080 4443 9801

USER 10000

#CMD [ "npm", "run-script", "start" ]
CMD ["/usr/bin/supervisord", "-n"]

